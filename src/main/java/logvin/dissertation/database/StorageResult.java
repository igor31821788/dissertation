package logvin.dissertation.database;

/**
 * <b>Модель записи таблицы</b>
 *
 * @author logvinIN
 * @since 22.06.2019
 */
public class StorageResult {
    /**
     * Идентификатор записи
     */
    private int id;

    /**
     * Гуид файла
     */
    private String guid;

    /**
     * Дата создания файла в виде строки
     */
    private String createDate;

    /**
     * Наименование файла-объекта копии
     */
    private String name;

    /**
     * Расширение файла-объекта копии
     */
    private String expansion;

    /**
     * Признак того, что копия создается "до" прогона сценария
     */
    private boolean createdBefore;

    StorageResult(int id, String guid, String createDate, String name, String expansion, boolean createdBefore) {
        this.id = id;
        this.guid = guid;
        this.createDate = createDate;
        this.name = name;
        this.expansion = expansion;
        this.createdBefore = createdBefore;
    }

    public int getId() {
        return id;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public String getExpansion() {
        return expansion;
    }

    public boolean isCreatedBefore() {
        return createdBefore;
    }
}
