package logvin.dissertation.utils;

/**
 * <b>Вспомогательная модель ошибки проекта</b>
 *
 * @author logvinIN
 * @since 22.06.2019
 */
public class DissertationException extends Exception {

    public DissertationException() {
        super("Необработанная ошибка");
    }

    public DissertationException(String message) {
        super(message);
    }


    public DissertationException(ExceptionModel exceptionModel) {
        super(exceptionModel.getCode() + ": " + exceptionModel.getMessage());
    }

    public DissertationException(ExceptionModel exceptionModel, String formatSpecificationValue) {
        super(exceptionModel.getCode() + ": " + exceptionModel.getMessage(formatSpecificationValue));
    }
}
