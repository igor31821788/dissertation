package logvin.dissertation.forms.tables;

import javafx.beans.property.*;
import javafx.scene.control.CheckBox;

/**
 * @author logvinIN
 * @since 06.07.2019
 */
public class CellWithRange extends CellAddress {

    /**
     * Является ли ячейка зависимой
     */
    private ObjectProperty<CheckBox> dependentCell;
    /**
     * Строковой атрибут колонки таблицы
     */
    private StringProperty rangeProperty;

    private StringProperty dependentRangeProperty;

    private StringProperty depRangeValRangeProperty;

    private boolean onlyRecalculationInRange;

    public CellWithRange(StringProperty cellAddressProperty,
                         ObjectProperty<CheckBox> dependentCellProperty,
                         StringProperty rangeProperty,
                         StringProperty dependentRangeProperty,
                         StringProperty depRangeValRangeProperty,
                         boolean onlyRecalculationInRange) {
        super(cellAddressProperty);
        this.dependentCell = dependentCellProperty;
        this.rangeProperty = rangeProperty;
        this.dependentRangeProperty = dependentRangeProperty;
        this.depRangeValRangeProperty = depRangeValRangeProperty;
        this.onlyRecalculationInRange = onlyRecalculationInRange;
    }

    public CellWithRange(String address,
                         boolean isDependentCell,
                         String range,
                         String depRangeAddress,
                         String depRangeValRange,
                         boolean onlyRecalculationInRange) {
        super(new SimpleStringProperty(address));
        this.dependentCell = new SimpleObjectProperty<>(new CheckBox());
        this.dependentCell.getValue().setSelected(isDependentCell);
        this.rangeProperty = new SimpleStringProperty(range);
        this.dependentRangeProperty = new SimpleStringProperty(depRangeAddress);
        this.depRangeValRangeProperty = new SimpleStringProperty(depRangeValRange);
        this.onlyRecalculationInRange = onlyRecalculationInRange;
    }

    public String getRangeValue() {
        return rangeProperty.get();
    }

    public StringProperty rangeProperty() {
        return rangeProperty;
    }

    public String getDependentRange() {
        return dependentRangeProperty.get();
    }

    public StringProperty dependentRangeProperty() {
        return dependentRangeProperty;
    }

    public String getDepRangeValRange() {
        return depRangeValRangeProperty.get();
    }

    public StringProperty depRangeValRangeProperty() {
        return depRangeValRangeProperty;
    }


    public ObjectProperty<CheckBox> getDependentCellProperty() {
        return dependentCell;
    }

    public boolean isDependentCell() {
        return dependentCell.get().isSelected();
    }

    public boolean isOnlyRecalculationInRange() {
        return onlyRecalculationInRange;
    }
}
