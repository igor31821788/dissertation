package logvin.dissertation.forms.tables;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author logvinIN
 * @since 10.09.2019
 */
public class CellWithAnchor extends CellAddress {

    private StringProperty anchorRangeProperty;


    public CellWithAnchor(StringProperty cellAddressProperty,
                          StringProperty anchorRangeProperty) {
        super(cellAddressProperty);
        this.anchorRangeProperty = anchorRangeProperty;
    }

    public CellWithAnchor(String targetCell,
                          String anchorRange) {
        super(new SimpleStringProperty(targetCell));
        this.anchorRangeProperty = new SimpleStringProperty(anchorRange);
    }

    public String getAnchorRange() {
        return anchorRangeProperty.get();
    }

    public StringProperty anchorRangeProperty() {
        return anchorRangeProperty;
    }

    public void setAnchorRangeProperty(String anchorRangeProperty) {
        this.anchorRangeProperty.set(anchorRangeProperty);
    }
}
