package logvin.dissertation.forms.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import logvin.dissertation.forms.tables.CellAddress;
import logvin.dissertation.forms.utils.ControllerUtils;
import logvin.dissertation.scenarios.AllScenarios;
import logvin.dissertation.utils.DissertationException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * <b>Контроллер формы для сценария "Замена формул на значение"</b>
 *
 * @author logvinIN
 * @since 01.03.2019
 */
public class FormulaReplacing implements DissertationController, Initializable {

    /**
     * Кнопка "ОК"
     */
    public Button okButton;

    /**
     * Кнопка "Внести в таблицу"
     */
    public Button addRow;

    /**
     * Поле ввода диапазона ячеек для замены формул на их значения
     */
    public TextField range;

    /**
     * Таблица для хранения значений {@link #range}
     */
    public TableView<CellAddress> rangeTable;

    /**
     * Поле ввода наименования листа рабочей книги
     */
    public TextField sheetName_field;

    /**
     * Кнопка "Очистить таблицу"
     */
    public Button clearTable_button;

    /**
     * Кнопка "Очистить все"
     */
    public Button clearAll_button;

    /**
     * Контекстное меню для таблицы {@link #rangeTable}
     */
    public ContextMenu contextMenu;

    /**
     * Колонка таблицы {@link #rangeTable}
     */
    @FXML
    private TableColumn<CellAddress, String> rangeColumn;

    /**
     * Путь к файлу
     */
    private String filePath;

    private ControllerUtils utils = new ControllerUtils();

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        rangeColumn.setCellValueFactory(cellData -> cellData.getValue().cellAddressProperty());
        rangeTable.getItems().addAll(getTableData());
        rangeTable.setContextMenu(contextMenu);
    }

    public void runScenario() {
        try {
            List<String> rangeList = new ArrayList<>();
            for (int i = 0; i < rangeTable.getItems().size(); i++) {
                rangeList = rangeTable.getItems()
                        .stream()
                        .map(CellAddress::getCellAddressValue)
                        .collect(Collectors.toList());
            }
            new AllScenarios().copyRandomCellRange(filePath, sheetName_field.getText(), rangeList);
            utils.showEndScenarioWindow(filePath);
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка сценария \"Замена формул на значение\"",
                    ex.getMessage());
        }
    }

    public void addInTable() {
        rangeTable.getItems().add(new CellAddress(new SimpleStringProperty(range.getText())));
        range.clear();
    }

    public void deleteItem() {
        CellAddress selectedItem = rangeTable.getSelectionModel().getSelectedItem();
        rangeTable.getItems().remove(selectedItem);
    }

    public void clearTable() {
        rangeTable.getItems().clear();
    }

    public void clearAll() {
        range.clear();
        sheetName_field.clear();
        clearTable();
    }

    public ObservableList<CellAddress> getTableData() {
        return FXCollections.observableArrayList();
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
