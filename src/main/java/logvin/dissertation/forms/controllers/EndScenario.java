package logvin.dissertation.forms.controllers;

import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import logvin.dissertation.database.FileStorageService;
import logvin.dissertation.forms.utils.ControllerUtils;
import logvin.dissertation.utils.DissertationException;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * <b>Контроллер формы окончания сценария</b>
 *
 * @author logvinIN
 * @since 23.06.2019
 */
public class EndScenario implements Initializable {

    /**
     * Надпись завершения сценария
     */
    public Label endScenario_label;

    /**
     * Кнопка "Открыть файловое хранилище"
     */
    public Button openFileStorage_button;

    /**
     * Кнопка "Открыть итоговый документ"
     */
    public Button openResultDoc_button;

    /**
     * Путь к файлу для открытия
     */
    private String filePathToOpen;

    private ControllerUtils utils = new ControllerUtils();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        endScenario_label.setText("Сценарий завершен.\n" +
                "Вы можете открыть результат используя кнопку \"Открыть итоговый документ\"\n" +
                "или открыть файловое хранилище для анализа истории документов.");
    }

    /**
     * <b>Открывает форму файлового хранилища</b>
     */
    public void openFileStorage() {
        try {
            Parent loadedLoader = utils
                    .getLoaderWithSource("FileStorage.fxml")
                    .load();
            utils.showForm("FileStorage.fxml", loadedLoader);
        } catch (DissertationException | IOException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка открытия окна файлового хранилища",
                    ex.getMessage());
        }
    }

    /**
     * <b>Открывает итоговый документ</b>
     */
    public void openResultFile() {
        try {
            String[] fileName = filePathToOpen.split("/");
            utils.showAlertWindow(Alert.AlertType.INFORMATION,
                    "Операция открытия документа (окно можно закрыть)",
                    "Запущен процесс открытия документа с именем\n" + fileName[fileName.length - 1]);
            new FileStorageService().openFile(filePathToOpen, false);
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка открытия итогового документа",
                    ex.getMessage());
        }
    }

    public void setFilePathToOpen(String filePathToOpen) {
        this.filePathToOpen = filePathToOpen;
    }
}
