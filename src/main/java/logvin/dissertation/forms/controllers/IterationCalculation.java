package logvin.dissertation.forms.controllers;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import logvin.dissertation.forms.tables.CellAddress;
import logvin.dissertation.forms.tables.CellWithRange;
import logvin.dissertation.forms.utils.ControllerUtils;
import logvin.dissertation.scenarios.AllScenarios;
import logvin.dissertation.utils.DissertationException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * <b>Контроллер формы "Суммирование результата"</b>
 *
 * @author logvinIN
 * @since 05.11.2018
 */
public class IterationCalculation implements DissertationController, Initializable {

    /**
     * Кнопка "ОК"
     */
    public Button okButton;

    /**
     * Поле ввода адреса ячейки-итератора
     */
    public TextField iteratorField;

    /**
     * Кнопка "Внести в таблицу"
     */
    public Button addRow;

    /**
     * Поле ввода адреса ячейки-сумматора
     */
    public TextField sourceCell;

    /**
     * Таблица хранения значений {@link #sourceCell}
     */
    public TableView<CellWithRange> indexes;

    /**
     * Контекстное меню для таблицы {@link #indexes}
     */
    public ContextMenu contextMenu;

    /**
     * Поле ввода наименования листа рабочей книги
     */
    public TextField sheetName_field;

    /**
     * Кнопка "Очистить таблицу"
     */
    public Button clearTable_button;


    public CheckBox sumResult_checkbox;
    public CheckBox maxValue_checkbox;
    public CheckBox minValue_checkbox;
    public CheckBox allValues_checkbox;
    public CheckBox allResultTypes_checkbox;
    public TextField from_textField;
    public TextField to_textField;
    public Label table_label;
    public TableColumn<CellWithRange, String> depRangeValRange_column;
    public TableColumn<CellWithRange, String> dependentRange_column;
    public TableColumn<CellWithRange, CheckBox> isDependentCell_column;
    public CheckBox onlyRecalc_checkbox;

    /**
     * Колонка таблицы {@link #indexes}
     */
    @FXML
    private TableColumn<CellWithRange, String> sourceCells;

    /**
     * Колонка таблицы {@link #indexes}
     */
    @FXML
    private TableColumn<CellWithRange, String> range_column;

    private ControllerUtils utils = new ControllerUtils();

    private String filePath;

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        sourceCells.setCellValueFactory(cellData -> cellData.getValue().cellAddressProperty());
        range_column.setCellValueFactory(cellData -> cellData.getValue().rangeProperty());
        isDependentCell_column.setCellValueFactory(cellData -> cellData.getValue().getDependentCellProperty());
        dependentRange_column.setCellValueFactory(cellData -> cellData.getValue().dependentRangeProperty());
        depRangeValRange_column.setCellValueFactory(cellData -> cellData.getValue().depRangeValRangeProperty());
        indexes.setContextMenu(contextMenu);
        dependentRange_column.setCellFactory(TextFieldTableCell.forTableColumn());
        depRangeValRange_column.setCellFactory(TextFieldTableCell.forTableColumn());
        indexes.setItems(getTableData());
//        from_textField.setTextFormatter(new TextFormatter<Object>(change -> {
//            String newText = change.getControlNewText();
//            if(newText.startsWith("-")){
//
//            }
//        }));
        //todo доделать текстформаттер
        table_label.setText("Чтобы добавить зависимый диапазон и его границы,\n" +
                "выделите запись в таблице и добавьте значения \n" +
                "вручную в формате:\n" +
                "для диапазона ячеек - \"A1:A200;\";\n" +
                "для границы зав.яч. - \"10;25\"");
    }

    public void runScenario() {
        try {
            List<CellWithRange> sourceCells = new ArrayList<>();
            for (int i = 0; i < indexes.getItems().size(); i++) {
                sourceCells = indexes.getItems()
                        .stream()
                        .map(cell -> new CellWithRange(
                                cell.getCellAddressValue(),
                                cell.isDependentCell(),
                                cell.getRangeValue(),
                                cell.getDependentRange(),
                                cell.getDepRangeValRange(),
                                cell.isOnlyRecalculationInRange()
                        ))
                        .collect(Collectors.toList());
            }
            //todo контроль на выбранный чекбокс
            new AllScenarios().multiCalculationScenario(filePath,
                    sheetName_field.getText(),
                    iteratorField.getText(),
                    sourceCells,
                    sumResult_checkbox.isSelected(),
                    maxValue_checkbox.isSelected(),
                    minValue_checkbox.isSelected(),
                    allValues_checkbox.isSelected());
            utils.showEndScenarioWindow(filePath);
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка сценария \"Суммирование результата\"",
                    ex.getMessage());
        }
    }

    @Override
    public void addInTable() {
        indexes.getItems().add(new CellWithRange(
                new SimpleStringProperty(sourceCell.getText()),
                new SimpleObjectProperty<>(new CheckBox()),
                new SimpleStringProperty(concatRange()),
                new SimpleStringProperty(""),
                new SimpleStringProperty(""),
                onlyRecalc_checkbox.isSelected()
        ));
        from_textField.clear();
        to_textField.clear();
        onlyRecalc_checkbox.setSelected(false);
        sourceCell.clear();
    }

    private String concatRange() {
        //todo маска на диапазон
        return !from_textField.getText().isEmpty() && !to_textField.getText().isEmpty() ?
                from_textField.getText() + ";" + to_textField.getText() :
                "";
    }

    public void deleteItem() {
        CellAddress selectedItem = indexes.getSelectionModel().getSelectedItem();
        indexes.getItems().remove(selectedItem);
    }

    public void clearTable() {
        indexes.getItems().clear();
    }

    public void clearAll() {
        iteratorField.clear();
        sourceCell.clear();
        sheetName_field.clear();
        from_textField.clear();
        to_textField.clear();
        allResultTypes_checkbox.setSelected(false);
        selectAll();
        clearTable();
        onlyRecalc_checkbox.setSelected(false);
    }


    public ObservableList<CellWithRange> getTableData() {
        return FXCollections.observableArrayList();
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void selectAll() {
        if (allResultTypes_checkbox.isSelected()) {
            sumResult_checkbox.setSelected(true);
            maxValue_checkbox.setSelected(true);
            minValue_checkbox.setSelected(true);
            allValues_checkbox.setSelected(true);
        } else {
            sumResult_checkbox.setSelected(false);
            maxValue_checkbox.setSelected(false);
            minValue_checkbox.setSelected(false);
            allValues_checkbox.setSelected(false);
        }
    }
}
