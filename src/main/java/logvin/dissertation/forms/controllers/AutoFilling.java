package logvin.dissertation.forms.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import logvin.dissertation.forms.tables.CellAddress;
import logvin.dissertation.forms.utils.ControllerUtils;
import logvin.dissertation.scenarios.AllScenarios;
import logvin.dissertation.utils.DissertationException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * <b>Контроллер формы для сценария "Автозаполнение"</b>
 *
 * @author logvinIN
 * @since 19.12.2018
 */
public class AutoFilling implements DissertationController, Initializable {

    /**
     * Кнопка "ОК"
     */
    public Button okButton;

    /**
     * Поле ввода адреса ячейки с условием останова
     */
    public TextField conditionCell_field;

    /**
     * Кнопка "Внести в таблицу"
     */
    public Button addRow;

    /**
     * Поле ввода адреса ячейки, значение которой надо "продлить"
     */
    public TextField toIncreaseCell;

    /**
     * Таблица хранения значений поля {@link #toIncreaseCell}
     */
    @FXML
    public TableView<CellAddress> indexes;

    /**
     * Поле ввода наименования листа рабочей книги
     */
    public TextField sheetName_field;

    /**
     * Кнопка "Очистить таблицу"
     */
    public Button clearTable_button;

    /**
     * Кнопка "Очистить все"
     */
    public Button clearAll_button;

    /**
     * Контекстное меню для таблицы
     */
    public ContextMenu contextMenu;

    /**
     * Пункт "Удалить" контекстного меню
     */
    public MenuItem delete_menuItem;

    /**
     * Колонка таблицы {@link #indexes}
     */
    @FXML
    private TableColumn<CellAddress, String> toIncreaseCells;

    /**
     * Путь к файлу
     */
    private String filePath;

    private ControllerUtils utils = new ControllerUtils();

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        toIncreaseCells.setCellValueFactory(cellData -> cellData.getValue().cellAddressProperty());
        indexes.setItems(getTableData());
        indexes.setContextMenu(contextMenu);
    }

    public void runScenario() {
        try {
            List<String> rangeCells = new ArrayList<>();
            for (int i = 0; i < indexes.getItems().size(); i++) {
                rangeCells = indexes.getItems().stream().map(CellAddress::getCellAddressValue).collect(Collectors.toList());
            }
            new AllScenarios().autoFillingScenario(filePath, sheetName_field.getText(), rangeCells, conditionCell_field.getText());
            utils.showEndScenarioWindow(filePath);
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка сценария \"Автозаполнение\"",
                    ex.getMessage());
        }
    }

    @Override
    public void addInTable() {
        indexes.getItems().add(new CellAddress(new SimpleStringProperty(toIncreaseCell.getText())));
        toIncreaseCell.clear();
    }

    @Override
    public void deleteItem() {
        CellAddress selectedItem = indexes.getSelectionModel().getSelectedItem();
        indexes.getItems().remove(selectedItem);
    }

    @Override
    public void clearTable() {
        indexes.getItems().clear();
    }

    @Override
    public void clearAll() {
        conditionCell_field.clear();
        toIncreaseCell.clear();
        sheetName_field.clear();
        clearTable();
    }


    @Override
    public ObservableList<CellAddress> getTableData() {
        return FXCollections.observableArrayList();
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
