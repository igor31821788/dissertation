package logvin.dissertation;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;

/**
 * <b>Класс, с которого инициализируется главная форма</b>
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("forms/fxml/Main.fxml")));
        primaryStage.setTitle("СИСТЕМА ТАБЛИЧНОГО МОДЕЛИРОВАНИЯ ЭКОНОМИЧЕСКИХ ПРОЦЕССОВ");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
